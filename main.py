import numpy as np
import pandas as pd

class Point:
    def __init__(self, x, y):
        """
        A point specified by (x,y) coordinates in the cartesian plane
        """
        self.x = x
        self.y = y


class Polygon:
    def __init__(self, edges):
        """
        points: a numpy array of Points
        """
        self.edges = edges

    def _does_intersect(self, point, edge):
        """
        Does a ray along x-axis from the point intersect the edge?
        :param point: Point object
        :param edge: 2 Points tuple
        :return: boolean
        """
        # A is  lower than B
        a, b = (edge[0], edge[1]) if edge[0].y < edge[1].y else (edge[1], edge[0])

        # Copy values not to move the point too far
        # in case of match with a vertex
        p_x, p_y = (point.x, point.y)

        # Including top point but excluding the bot point
        # we can deal with the cases when the ray goes right through the point
        if p_y > b.y or p_y <= a.y or p_x > max(a.x, b.x):
            # The right horizontal ray does not intersect with the edge
            return False

        if p_x < min(a.x, b.x):  # The ray intersects with the edge
            return True

        # Now we know the point is inside the rectangle with the AB diagonal
        # We can check whether it lies  to the right or left of the AB line
        # By compare slopes of AP and AB
        # but first check for the infinite slopes cases
        if p_x == a.x:
            if a.x == b.x:
                return True  # The point is on the boundary - does it count?
            elif b.x < a.x:
                return False
            else:
                return True
        else:
            # There can't be the case that B.x == A.x but p_x != A.x
            # In that case the point would have been left or right
            # and the function would have returned earlier
            slope_edge = (b.y - a.y) / (b.x - a.x)
            slope_point = (p_y - a.y) / (p_x - a.x)

        if slope_point >= slope_edge:
            # The ray intersects with the edge
            return True

    def contains(self, point):
        # We start on the outside of the polygon
        inside = False
        for edge in self.edges:
            if self._does_intersect(point, edge):
                inside = not inside

        return inside


class Task:
    def __init__(self, user_coords_file, place_coords_file, out_file):
        self.user_coords_file = user_coords_file
        self.place_coords_file = place_coords_file
        self.out_file = out_file
        self.user_points = self._initialize_user_points()
        self.polygons = self._initialize_polygons()

    def _initialize_user_points(self):
        points_array = pd.read_csv(self.user_coords_file).to_numpy()
        # Create a list of tuples (user_id, Point)
        # to add readability
        points_list = [(p[0], Point(p[1], p[2])) for p in points_array]
        return points_list

    def _initialize_polygons(self):
        # Read input file
        df_polygons = pd.read_csv(self.place_coords_file)

        # Group by place id to separate polygons
        g = df_polygons.groupby('place_id').cumcount()
        polygons_array = df_polygons.set_index(['place_id', g]).unstack().stack().groupby(level=0).apply(
            lambda x: x.values).to_numpy()

        return self._make_list_of_polygons(polygons_array)

    def _make_list_of_polygons(self, polygons_array):
        # Make edges a list of Points to add readability
        polygons = []
        for polygon in polygons_array:
            # Sort by point number
            # not to assume that they are always sorted
            polygon = polygon[polygon[:, 2].argsort()][:, :2]
            # Shift points and concatenate to create array of edges
            # by connecting adjacent vertices
            edges_array = np.stack((polygon, np.roll(polygon, 1, axis=0)), axis=1)
            # Make the edges a list of Points to add readability
            edges_list = [(Point(edge[0][0], edge[0][1]), Point(edge[1][0], edge[1][1])) for edge in edges_array]
            polygons.append(Polygon(edges_list))

        return polygons

    def solve(self):
        # Create a result array
        # not to iteratively append to a DataFrame
        # because it copies all the data
        result = np.empty((len(self.user_points), 2), dtype=int)
        for i, (id, point) in enumerate(self.user_points):
            places_available = 0
            for p in self.polygons:
                if p.contains(point):
                    places_available += 1

            result[i] = [id, places_available]

        return result

    def dump_result(self, result):
        df_out = pd.DataFrame(columns=['id', 'number_of_places_available'], data=result)
        df_out.to_csv(self.out_file, index=False)


if __name__ == '__main__':
    task = Task('user_coordinates.csv', 'place_zone_coordinates.csv', 'out.csv')
    result = task.solve()
    task.dump_result(result)
